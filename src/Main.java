import java.util.ArrayList;

import engine.SearchEngine;
import engine.SearchParameters;
import engine.filters.*;
import model.CarOffer;


public class Main {

	public static void main(String[] args) {

		CarOffer bmw = new CarOffer();
		bmw.setBrand("BMW");
		bmw.setPrice(20000);
		CarOffer toyota = new CarOffer();
		toyota.setBrand("toyota");
		toyota.setPrice(15000);
		CarOffer alfa = new CarOffer();
		alfa.setBrand("alfa romeo");
		alfa.setPrice(12000);
		ArrayList<CarOffer> offers = new ArrayList<CarOffer>();
		offers.add(alfa);
		offers.add(toyota);
		offers.add(bmw);
		
		Filter brandFilter = new BrandFilter();
		Filter priceFilter = new PriceFilter();
		
		SearchEngine engine =new SearchEngine();
		
		engine.addFilter(priceFilter);
		engine.addFilter(brandFilter);
		
		SearchParameters parameters = new SearchParameters();
		parameters.setMinimalPrice(13000);
		parameters.setMaximalPrice(19000);
		engine.setParameters(parameters);
		engine.setOfferList(offers);
		ArrayList<CarOffer> result = engine.getResults();
		
		for(CarOffer offer: result){
			System.out.println(offer.getBrand()+" "+offer.getPrice());
		}

	}

}
