package engine;

import java.util.Date;

public class SearchParameters {

	private String brand;
	private int minimalPrice;
	private int maximalPrice;
	private int minimalMileage;
	private int maximalMileage;
	private int minimalProductionYear;
	private int maximalProductionYear;
	private String region;
	private String city;
	private boolean damaged;
	private boolean unused;
	private Date minimalDate;
	private Date maximalDate;
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public int getMinimalPrice() {
		return minimalPrice;
	}
	public void setMinimalPrice(int minimalPrice) {
		this.minimalPrice = minimalPrice;
	}
	public int getMaximalPrice() {
		return maximalPrice;
	}
	public void setMaximalPrice(int maximalPrice) {
		this.maximalPrice = maximalPrice;
	}
	public int getMinimalMileage() {
		return minimalMileage;
	}
	public void setMinimalMileage(int minimalMileage) {
		this.minimalMileage = minimalMileage;
	}
	public int getMaximalMileage() {
		return maximalMileage;
	}
	public void setMaximalMileage(int maximalMileage) {
		this.maximalMileage = maximalMileage;
	}
	public int getMinimalProductionYear() {
		return minimalProductionYear;
	}
	public void setMinimalProductionYear(int minimalProductionYear) {
		this.minimalProductionYear = minimalProductionYear;
	}
	public int getMaximalProductionYear() {
		return maximalProductionYear;
	}
	public void setMaximalProductionYear(int maximalProductionYear) {
		this.maximalProductionYear = maximalProductionYear;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public boolean isDamaged() {
		return damaged;
	}
	public void setDamaged(boolean damaged) {
		this.damaged = damaged;
	}
	public boolean isUnused() {
		return unused;
	}
	public void setUnused(boolean unused) {
		this.unused = unused;
	}
	public Date getMinimalDate() {
		return minimalDate;
	}
	public void setMinimalDate(Date minimalDate) {
		this.minimalDate = minimalDate;
	}
	public Date getMaximalDate() {
		return maximalDate;
	}
	public void setMaximalDate(Date maximalDate) {
		this.maximalDate = maximalDate;
	}
	
	
	
}
