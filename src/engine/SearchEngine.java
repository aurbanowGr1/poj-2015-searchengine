package engine;

import java.util.ArrayList;

import engine.filters.Filter;
import model.CarOffer;

public class SearchEngine {

	private ArrayList<CarOffer> offerList;
	private SearchParameters parameters;
	private ArrayList<Filter> filters = new ArrayList<Filter>();
	
	public void addFilter(Filter filter){
		filters.add(filter);
	}
	
	public void removeFilter(Filter filter){
		filters.remove(filter);
	}
	
	public ArrayList<CarOffer> getOfferList() {
		return offerList;
	}
	public void setOfferList(ArrayList<CarOffer> offerList) {
		this.offerList = offerList;
	}
	public SearchParameters getParameters() {
		return parameters;
	}
	public void setParameters(SearchParameters parameters) {
		this.parameters = parameters;
		for(Filter filter:filters){
			filter.setParameters(parameters);
		}
	}
	
	public ArrayList<CarOffer> getResults(){
		
		if(offerList==null || offerList.isEmpty())return new ArrayList<CarOffer>();
		if(parameters==null) return offerList;
		ArrayList<CarOffer> result = new ArrayList<CarOffer>(offerList);
		for(Filter filter: filters){
			if(filter.canApplyFilter())
				filter.applyFilter(result);
		}
		return result;
	}
}
