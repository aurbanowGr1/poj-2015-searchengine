package engine.filters;

import java.util.ArrayList;

import model.CarOffer;
import engine.SearchParameters;

public class PriceFilter implements Filter{

	SearchParameters parameters;
	@Override
	public SearchParameters getParameters() {
		return parameters;
	}

	@Override
	public void setParameters(SearchParameters parameters) {
		this.parameters=parameters;
	}

	@Override
	public boolean canApplyFilter() {
		return parameters!=null
				&&(parameters.getMaximalPrice()>0 
						|| parameters.getMinimalPrice()>0);
	}

	@Override
	public void applyFilter(ArrayList<CarOffer> offers) {
		if(!canApplyFilter())return;
		ArrayList<CarOffer> tmp = new ArrayList<CarOffer>(offers);
		for(CarOffer offer: tmp){
			if(parameters.getMinimalPrice()>0)
				if(offer.getPrice()<parameters.getMinimalPrice())
					offers.remove(offer);
			
			if(parameters.getMaximalPrice()>0)
				if(offer.getPrice()>parameters.getMaximalPrice())
					offers.remove(offer);
		}
		
	}

}
