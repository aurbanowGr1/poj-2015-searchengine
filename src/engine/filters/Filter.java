package engine.filters;

import java.util.ArrayList;

import model.CarOffer;
import engine.SearchParameters;

public interface Filter {
	public SearchParameters getParameters();
	public void setParameters(SearchParameters parameters);
	public boolean canApplyFilter();
	public void applyFilter(ArrayList<CarOffer> offers);
}
