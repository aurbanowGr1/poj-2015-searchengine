package engine.filters;

import java.util.ArrayList;

import model.CarOffer;
import engine.SearchParameters;

public class BrandFilter implements Filter{

	SearchParameters parameters;
	@Override
	public SearchParameters getParameters() {
		return parameters;
	}

	@Override
	public void setParameters(SearchParameters parameters) {
		this.parameters =parameters;
	}

	@Override
	public boolean canApplyFilter() {
		return parameters!=null
				&& parameters.getBrand()!=null
				&& !parameters.getBrand().equals("");
	}

	@Override
	public void applyFilter(ArrayList<CarOffer> offers) {
		if(!canApplyFilter())return;
		ArrayList<CarOffer> tmp = new ArrayList<CarOffer>(offers);
		for(CarOffer offer: tmp){
			if(!offer.getBrand().equalsIgnoreCase(parameters.getBrand()))
				offers.remove(offer);
		}
	}

}
